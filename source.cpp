#include <stdio.h>
#include <SDL2/SDL.h>
#include <math.h>

// Adjustable parameters
const int screenWidth = 800;
const int screenHeight = 600;
const double xLim[2] = { -5, 5 };
const double yLim[2] = { -25, 25 };
const int dataPoints = 1000;

const double xOffset = screenWidth / 2;
const double yOffset = screenHeight / 2;
const double xScale = screenWidth / (xLim[1] - xLim[0]);
const double yScale = screenHeight / (yLim[1] - yLim[0]);

// The function to plot
double f(double x)
{
	return -pow(x, 4) + 9*pow(x, 2) + 4*x - 12;
}

// Returns an int array of linearly spaced values
double* getXData(double start, double end)
{
	double* xData = new double[dataPoints + 1];
	double step = (end - start) / dataPoints;
	double currentValue = start;

	for (int i = 0; i <= dataPoints; i++)
	{
		xData[i] = currentValue;
		currentValue += step;
	}

	return xData;
}

// Calculate y points for every x point
double* getYData(double* xData, int numElements)
{
	double* yData = new double[numElements + 1];
	for (int i = 0; i <= numElements; i++)
	{
		yData[i] = f(xData[i]);
	}

	return yData;
}

// Draws a line with added scale, offset and inverts the y axis
void drawLine(SDL_Renderer* renderer, double x1, double y1, double x2, double y2)
{
	int x1N = int(x1 * xScale + xOffset);
	int y1N = int(-y1 * yScale + yOffset);
	int x2N = int(x2 * xScale + xOffset);
	int y2N = int(-y2 * yScale + yOffset);
	SDL_RenderDrawLine(renderer, x1N, y1N, x2N, y2N);
}

void drawXYAxis(SDL_Renderer* renderer)
{
	// Draw two strait lines through origin
	drawLine(renderer, xLim[0], 0, xLim[1], 0);
	drawLine(renderer, 0, yLim[0], 0, yLim[1]);

	// Draw sublines
	// 1 pixel for each increment
	// 3 pixels every five increments
	// 5 pixels every ten increments

	// Sublines for x
	int step = int(floor(xLim[0]));
	int end = int(ceil(xLim[1]));
	for (step; step < end; step++)
	{
		if (step % 10 == 0) drawLine(renderer, step, 0.5, step, -0.5);
		else if (step % 5 == 0) drawLine(renderer, step, 0.3, step, -0.3);
		else drawLine(renderer, step, 0.1, step, -0.1);
	}

	// Sublines for y
	step = floor(yLim[0]);
	end = ceil(yLim[1]);
	for (step; step < end; step++)
	{
		if (step % 10 == 0) drawLine(renderer, 0.5, step, -0.5, step);
		else if (step % 5 == 0) drawLine(renderer, 0.3, step, -0.3, step);
		else drawLine(renderer, 0.1, step, -0.1, step);
	}


}

// Draws the function with the given renderer
void plotFunction(SDL_Renderer* renderer, double* xData, double* yData)
{
	double xOffset = screenWidth / 2;
	double yOffset = screenHeight / 2;
	double xScale = screenWidth / (xLim[1] - xLim[0]);
	double yScale = screenHeight / (yLim[1] - yLim[0]);

	for (int i = 0; i < dataPoints; i++)
	{
		drawLine(renderer, xData[i], yData[i], xData[i+1], yData[i+1]);
	}
}

int main(int argc, char * argv[])
{
	double* xData = getXData(xLim[0], xLim[1]);
	double* yData = getYData(xData, dataPoints);

	SDL_Init(SDL_INIT_EVERYTHING);
	SDL_Window* window = SDL_CreateWindow("SDL project", 
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 
		screenWidth, screenHeight, NULL);
	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, NULL);
	SDL_Event event;

	while (true)
	{
		SDL_PollEvent(&event);

		switch (event.type)
		{
		case SDL_QUIT:
			return 0;
		default:
			break;
		}

		// Draw black background
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
		SDL_RenderClear(renderer);

		// Set renderer white for drawing axis
		SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
		drawXYAxis(renderer);

		// Set renderer another color for plotting function
		SDL_SetRenderDrawColor(renderer, 0, 164, 0, 255);
		plotFunction(renderer, xData, yData);
		
		// Update screen
		SDL_RenderPresent(renderer);
	}

	return 0;
}