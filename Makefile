# Source file to compile
SOURCE_FILE = source.cpp

# Name of the resulting executable file
EXECUTABLE_NAME = Graphplot

all : $(SOURCE_FILE)
	g++ $(SOURCE_FILE) -w -lSDL2 -o $(EXECUTABLE_NAME)